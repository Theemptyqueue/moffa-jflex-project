package comboscanner;

import comboscanner.ScannerType;
import comboscanner.Token;
import java.util.HashMap;

/**
 *
 * @author John David Moffa
 */
public class SimpleScanner {

    // CONSTANTS: Input states.
    private static final int START = 0;
    private static final int IN_ID = 1;
    private static final int IN_NUM = 2;
    private static final int IN_COLON = 3;
    private static final int IN_LESS_THAN = 4;
    private static final int IN_CHEESE_GRATER_THEN = 5;

    // Error value that when met will ends the scanner at any point in the input stream
    //if any input states have a greater value, the will have the effect of putting this scanner into an infinite loop.
    private static final int ERROR = 10;

    //complete states
    //end states for the scanner
    //all values for the end states must be greater than the error value to avoid it being an input state.
    private static final int ID_COMPLETE = 11;
    private static final int NUM_COMPLETE = 12;
    private static final int SYMBOL_COMPLETE = 13;
    private static final int LESS_THAN_COMPLETE = 15;
    private static final int CHEESE_GRATER_THEN_COMPLETE = 16;
    private static final int COLON_COMPLETE = 14;

    // Instance variables
    private String theSource = "";
    private int currentIndex = 0;
    protected HashMap<String, TokenType> lookupTable = new HashMap<String, TokenType>();

    // Constructor
    public SimpleScanner(String toScan) {
        this.theSource = toScan;

        //tokens
        /*Added: 
         + addition
         - subtraction
         * multiplication
         / integer division
         ; 
         :
         = logical equals
         mod modulo
         div float division
         to the lookup table*/
        lookupTable.put("+", TokenType.PLUS);
        lookupTable.put("-", TokenType.MINUS);
        lookupTable.put("*", TokenType.MULTIPLY);
        lookupTable.put("/", TokenType.DIVIDES);
        lookupTable.put(";", TokenType.SEMI);
        lookupTable.put(":", TokenType.COLON);
        lookupTable.put("=", TokenType.ASSIGN);
        lookupTable.put(">", TokenType.CHEESE_GRATER_THEN);
        lookupTable.put("<", TokenType.LESS_THAN);
        lookupTable.put("mod", TokenType.MOD);
        lookupTable.put("div", TokenType.DIV);

        /*Added: 
         left parenthesis to lookupTable 
         right parenthesis to lookupTable
         left square bracket to lookupTable 
         right square bracket to lookupTable
         left curly bracket to lookupTable
         right curly bracket to lookupTable*/
        lookupTable.put("(", TokenType.LEFTPAREN);
        lookupTable.put(")", TokenType.RIGHTPAREN);
        lookupTable.put("[", TokenType.LEFTSQUARE);
        lookupTable.put("]", TokenType.RIGHTSQUARE);
        lookupTable.put("{", TokenType.LEFTCURLY);
        lookupTable.put("}", TokenType.RIGHTCURLY);

        // adding basic logical operators.
		/*Added:
         and logical and
         not logical not
         or logical or
         if logical if condition
         then implied from the if condition
         else logical else condition
         */
        lookupTable.put("and", TokenType.AND);
        lookupTable.put("not", TokenType.NOT);
        lookupTable.put("or", TokenType.OR);
        lookupTable.put("if", TokenType.IF);
        lookupTable.put("then", TokenType.THEN);
        lookupTable.put("else", TokenType.ELSE);

        //adding complex operators
		/*Added:
         >= greater than or equals operator
         <= less than or equals operator
         <> not equal operator
         := assignment operator
         */
        lookupTable.put(">=", TokenType.CHEESE_GRATER_THEN_EQUAL);
        lookupTable.put("<=", TokenType.LESS_THEN_EQUAL);
        lookupTable.put("<>", TokenType.NOT_EQUAL_TO);
        lookupTable.put(":=", TokenType.ASSIGNMENT);

        //adding declarations.
        lookupTable.put("program", TokenType.PROGRAM);
        lookupTable.put("integer", TokenType.INTEGER);
        lookupTable.put("real", TokenType.REAL);
        lookupTable.put("array", TokenType.ARRARY);
        lookupTable.put("of", TokenType.OF);
        lookupTable.put("function", TokenType.FUNCTION);
        lookupTable.put("procedure", TokenType.PROCEDURE);
        lookupTable.put("begin", TokenType.BEGIN);
        lookupTable.put("end", TokenType.END);
        lookupTable.put("while", TokenType.WHILE);

        lookupTable.put("do", TokenType.DO);
        lookupTable.put("var", TokenType.VAR);
    }

    // Instance functions
    public Token nextToken() {
        Token answer = null;
        // if there is no more string to consume, return null
        if (currentIndex >= theSource.length()) {
            return null;
        }
        // creating a lexeme string that is used to check if the input string is empty or has characters
        String lexeme = "";
        ScannerType whichScanner = ScannerType.SimpleScanner;
        // Start in start state
        int currentState = SimpleScanner.START;

        if (whichScanner == ScannerType.SimpleScanner) {
            while (currentState < SimpleScanner.ERROR) {
            //System.out.println("While index is " + currentIndex + " and state is " + currentState);
                // By default the char is whitespace
                char currentChar = ' ';
                if (currentIndex >= theSource.length() && currentState != 0) {
                    currentChar = ' ';//there is no current character
                } else if (currentIndex < theSource.length()) {
                    currentChar = theSource.charAt(currentIndex);
                } else {
                    break;
                }
                //System.out.println("The current chareacter is: " + currentChar + "\nThe current state is :" + currentState);
                switch (currentState) {

                    case SimpleScanner.START:
                        if (Character.isLetter(currentChar)) {
                            lexeme = lexeme + currentChar;
                            currentState = SimpleScanner.IN_ID;
                            currentIndex++;
                        } else if (Character.isDigit(currentChar)) {
                            lexeme = lexeme + currentChar;
                            currentState = SimpleScanner.IN_NUM;
                            currentIndex++;
                        } else if (Character.isWhitespace(currentChar)) {
                            currentIndex++;
                        } else if (currentChar == '+'
                                || currentChar == '-'
                                || currentChar == '*'
                                || currentChar == '/'
                                || currentChar == ';'
                                || currentChar == '='
                                || currentChar == '('
                                || currentChar == ')'
                                || currentChar == '['
                                || currentChar == ']'
                                || currentChar == '{'
                                || currentChar == '}') {
                            currentIndex++;
                            currentState = SimpleScanner.SYMBOL_COMPLETE;
                            lexeme = lexeme + currentChar;

                            //check for a single :
                        } else if (currentChar == ':') {
                            System.out.println("I found a colon");
                            lexeme = lexeme + currentChar;
                            currentState = SimpleScanner.IN_COLON;
                            currentIndex++;
                        } else if (currentChar == '<') {
                            lexeme = lexeme + currentChar;
                            currentState = SimpleScanner.IN_LESS_THAN;
                            currentIndex++;
                        } else if (currentChar == '>') {
                            lexeme = lexeme + currentChar;
                            currentState = SimpleScanner.IN_CHEESE_GRATER_THEN;
                            currentIndex++;
                        } else {
                            System.out.println("We found an Error.");
                            currentIndex++;
                            currentState = SimpleScanner.ERROR;
                            lexeme = lexeme + currentChar;
                        }
                        break;

                    case SimpleScanner.IN_ID:
                        if (Character.isLetterOrDigit(currentChar)) {
                            lexeme = lexeme + currentChar;
                            currentIndex++;
                        } else {
                            currentState = SimpleScanner.ID_COMPLETE;
                        }
                        break;
                    //check for a >= in the input string
                    case SimpleScanner.IN_LESS_THAN:
                        if (currentChar == '>' || currentChar == '=') {
                            lexeme = lexeme + currentChar;
                            currentIndex++;
                        } else {
                            currentState = SimpleScanner.LESS_THAN_COMPLETE;
                        }
                        break;

                    //check for a logical equal in the input string
                    case SimpleScanner.IN_CHEESE_GRATER_THEN:
                        if (currentChar == '=') {
                            lexeme = lexeme + currentChar;
                            currentIndex++;
                        } else {
                            currentState = SimpleScanner.CHEESE_GRATER_THEN_COMPLETE;
                        }
                        break;
                    //check for a := in the input string
                    case SimpleScanner.IN_COLON:
                        System.out.println("I am in the colon case");
                        if (currentChar == '=') {
                            lexeme = lexeme + currentChar;
                            System.out.println("I found an = after the :");
                            currentIndex++;
                        } else {
                            System.out.println("I completed the colon");
                            currentState = SimpleScanner.COLON_COMPLETE;
                        }
                        break;
                    //check to t see if the current element in the input string is a character or a number.
                    case SimpleScanner.IN_NUM:
                        if (Character.isDigit(currentChar)) {
                            lexeme = lexeme + currentChar;
                            currentIndex++;
                        } else {
                            currentState = SimpleScanner.NUM_COMPLETE;
                        }
                        break;

                    default:
                        System.out.println("The current character is " + currentChar + "\nThe current state is : " + currentState);
                        System.out.println("Never get here");
                        break;
                } /// end switch
            } // end while
        } else {
            whichScanner = ScannerType.JFLEXSCANNER;

        }
        // Now we are in a completion state:
        //this is the error state that is entered into when an input value is to large
        switch (currentState) {
            case ERROR:
                answer = new Token(lexeme, null);
                break;

            case ID_COMPLETE:
                TokenType idToken = lookupTable.get(lexeme);
                if (idToken == null) {
                    idToken = TokenType.ID;
                }
                answer = new Token(lexeme, idToken);
                break;
            //I found a <= operator
            case LESS_THAN_COMPLETE:
                TokenType lecToken = lookupTable.get(lexeme);
                if (lecToken == null) {
                    lecToken = TokenType.LESS_THAN;
                }
                answer = new Token(lexeme, lecToken);
                break;
            //I found a >= operator
            case CHEESE_GRATER_THEN_COMPLETE:
                TokenType cgecToken = lookupTable.get(lexeme);
                if (cgecToken == null) {
                    cgecToken = TokenType.CHEESE_GRATER_THEN;
                }
                answer = new Token(lexeme, cgecToken);
                break;

            case COLON_COMPLETE:
                TokenType acToken = lookupTable.get(lexeme);
                if (acToken == null) {
                    acToken = TokenType.ASSIGN;
                }
                answer = new Token(lexeme, acToken);
                break;

            case NUM_COMPLETE:
                answer = new Token(lexeme, TokenType.NUMBER);
                break;

            case SYMBOL_COMPLETE:
                TokenType whichToken = lookupTable.get(lexeme);
                answer = new Token(lexeme, whichToken);
                break;
        }

        return answer;
    }
    /**
     * 
     * @param lexeme is the input string
     * @return  the token answer.
     * Using the lookup table this assigns a token type based on the string representation.
     */ 
    public Token assignToken(String lexeme) {
        Token answer = null;

        TokenType Token = lookupTable.get(lexeme);
        if (Token == null) {
            Token = TokenType.ID;
        }
        answer = new Token(lexeme, Token);
        
        return answer;
    }
}
