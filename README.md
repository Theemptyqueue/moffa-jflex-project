John David Moffa

This is the README for the Jflex project started in CSC 450 on 12/12/2018

The goal of this project is to create a pascal scanner using the Java language.

How to run the program for iteration 1:

run the jflex tool on standalonev1.jflex

compile the generated code using the javac command

run the compiled class file in the command line or and IDE with a input file as the argument.


How to run the program for iteration 2:

Run the jflex tool on scannerdependantv1.jflex

Run the javac command on SimpleScannerMain.java

In the package for iteration 2, include the following classes:

SimpleScanner.java
SimpleScannerMain.java
Token.java
TokenType.java
ScannerType.java
DependantScannerv1.java - this is the file created by the jflex tool for iteration 2.

Run the program using SimpleScannerMain using command-line arguments similar to iteration 1.