/**
 * John David Moffa
 * This is a simple example of a jflex lexer definition
 * This is a dependent scanner that has no built in main class
 */

/* Declarations */
%%


%class  DependantScannerv1   /* Names the produced java file */
%function nextToken /* Renames the yylex() function */
%type   String      /* Defines the return type of the scanning function */
%eofval{
  return null;
%eofval}
/* Patterns */


/*Regular Expressions*/
/*extra characters not valid in the mini-pascal*/
other         = .

/*token = begin*/
begin = begin
/*token = end*/
end = end
/*token = and*/
and = and
/*token = or*/
or = or
/*token = not*/
not = not
/*token = mod*/
modulo = mod
/*token = if*/
if = if
/*token = then*/
then = then
/*token = else*/
else = else
/*token = do*/
do = do
/*token = while*/
while = while
/*token = div*/
div = div
/*token = var*/
var = var
/*token = array*/
array = array
/*token = real*/
real = real
/*token = integer*/
integer = integer
/*token = program*/
program = program
/*token = procedure*/
procedure = procedure
/*token = function*/
function = function
/*token = of*/
of = of

letter        = [A-Za-z]
word          = {letter}+
number 		  = [0-9]+
whitespace    = [\x20|\a|\n|\t|\r\n|\r|\n\r|\025]
/*token = comma*/
comma 	  = ,
/*token = colon*/
colon = :
/*token = semicolon*/
semicolon = ;
/*token = greater*/
greater = >
/*token = less*/
less = <
/*token = notequal*/
notequal = <>
/*token = equals*/
equals = \=
/*token = plus*/
plus = \+
/*token = minus*/
minus = \-
/*token = times*/
times  = \*
/*token = divides*/
divides = \/
/*token = leftcurly*/
leftcurly = \{
/*token = rightcurly*/
rightcurly = \}
/*token = leftbracket*/
leftbracket = \[
/*token = rightbracket*/
rightbracket = \]
/*token = leftparen*/
leftparen = \(
/*token = leftparen*/
rightparen = \)
/*token = lessequal*/
lessequal	= <\=
/*token = greaterequal*/
greaterequal = >\=
/*token = assignment*/
assignment = :\=
/*token = dot*/
dot = \.
/*token = singlequote*/
singlequote = \'
/*token = doublequote*/
doublequote = \"

%%
/* Lexical Rules */
/**
*/
{begin}		{ 
             System.out.println("Found a begin: '" + yytext() + "' found.");
             return ( yytext());
           }
{end}		{ 
             System.out.println("Found a end: '" + yytext() + "' found.");
             return ( yytext());
           }
{and}		{ 
             System.out.println("Found a logical and: '" + yytext() + "' found.");
             return ( yytext());
           }
{or}		{ 
             System.out.println("Found a logical or: '" + yytext() + "' found.");
             return ( yytext());
           }
{not}		{ 
             System.out.println("Found a logical not: '" + yytext() + "' found.");
             return ( yytext());
           }
{modulo}		{ 
             System.out.println("Found a mod: '" + yytext() + "' found.");
             return ( yytext());
           }
{if}		{ 
             System.out.println("Found a if: '" + yytext() + "' found.");
             return ( yytext());
           }		   
{then}		{ 
             System.out.println("Found a then: '" + yytext() + "' found.");
             return ( yytext());
           }		   
{else}		{ 
             System.out.println("Found a else: '" + yytext() + "' found.");
             return ( yytext());
           }
{do}		{ 
             System.out.println("Found a do: '" + yytext() + "' found.");
             return ( yytext());
           }		   
{while}		{ 
             System.out.println("Found a while: '" + yytext() + "' found.");
             return ( yytext());
           }
{div}		{ 
             System.out.println("Found a div: '" + yytext() + "' found.");
             return ( yytext());
           }
{var}		{ 
             System.out.println("Found a var: '" + yytext() + "' found.");
             return ( yytext());
           }
{array}		{ 
             System.out.println("Found a array: '" + yytext() + "' found.");
             return ( yytext());
           }
{real}		{ 
             System.out.println("Found a real: '" + yytext() + "' found.");
             return ( yytext());
           }		   
{integer}		{ 
             System.out.println("Found a integer: '" + yytext() + "' found.");
             return ( yytext());
           }
{program}		{ 
             System.out.println("Found a program: '" + yytext() + "' found.");
             return ( yytext());
           }
{procedure}		{ 
             System.out.println("Found a procedure: '" + yytext() + "' found.");
             return ( yytext());
           }
{function}		{ 
             System.out.println("Found a function: '" + yytext() + "' found.");
             return ( yytext());
           }
{of}		{ 
             System.out.println("Found a of: '" + yytext() + "' found.");
             return ( yytext());
           }

		   
{word}     {
             /** Print out the word that was found. */
             System.out.println("Found a word: " + yytext());
             return( yytext());
            }
{number}     {
             /** Print out the word that was found. */
             System.out.println("Found a number: " + yytext());
             return( yytext());
            }
            
{whitespace}  {  /* Ignore Whitespace */ 
                 return "";
              }
/*Creates a function to find commas*/           


{comma}    { 
             System.out.println("Found a comma: '" + yytext() + "' found.");
             return ( yytext());
           }
{colon}    { 
             System.out.println("Found a colon: '" + yytext() + "' found.");
             return ( yytext());
           }
{semicolon}    { 
             System.out.println("Found a semicolon: '" + yytext() + "' found.");
             return ( yytext());
           } 
{greater}    { 
             System.out.println("Found a greater: '" + yytext() + "' found.");
             return ( yytext());
           }
{less}    { 
             System.out.println("Found a less: '" + yytext() + "' found.");
             return ( yytext());
           }
{notequal}    { 
             System.out.println("Found a notequal: '" + yytext() + "' found.");
             return ( yytext());
           }
{equals}    { 
             System.out.println("Found a equals: '" + yytext() + "' found.");
             return ( yytext());
           }
{leftcurly}    { 
             System.out.println("Found a leftcurly: '" + yytext() + "' found.");
             return ( yytext());
           }
{rightcurly}    { 
             System.out.println("Found a rightcurly: '" + yytext() + "' found.");
             return ( yytext());
           }
{leftbracket}    { 
             System.out.println("Found a leftbracket: '" + yytext() + "' found.");
             return ( yytext());
           }
{rightbracket}    { 
             System.out.println("Found a rightbracket: '" + yytext() + "' found.");
             return ( yytext());
           }
{leftparen}    { 
             System.out.println("Found a leftparen: '" + yytext() + "' found.");
             return ( yytext());
           }
{rightparen}    { 
             System.out.println("Found a rightparen: '" + yytext() + "' found.");
             return ( yytext());
           }
{plus}    { 
             System.out.println("Found a add: '" + yytext() + "' found.");
             return ( yytext());
           }
{minus}    { 
             System.out.println("Found a subtract: '" + yytext() + "' found.");
             return ( yytext());
           }
{times}    { 
             System.out.println("Found a multiply: '" + yytext() + "' found.");
             return ( yytext());
           }
{divides}    { 
             System.out.println("Found a divides: '" + yytext() + "' found.");
             return ( yytext());
           }
{lessequal}    { 
             System.out.println("Found a less than or equals: '" + yytext() + "' found.");
             return ( yytext());
           }
{greaterequal}    { 
             System.out.println("Found a greater than or equals: '" + yytext() + "' found.");
             return ( yytext());
           }		   
{dot}		{ 
             System.out.println("Found a dot: '" + yytext() + "' found.");
             return ( yytext());
           }
{assignment}		{ 
             System.out.println("Found a assignment: '" + yytext() + "' found.");
             return ( yytext());
           }
{singlequote}		{ 
             System.out.println("Found a character: '" + yytext() + "' found.");
             return ( yytext());
           }
{doublequote}		{ 
             System.out.println("Found a string: '" + yytext() + "' found.");
             return ( yytext());
           }		   
{other}    { 
             System.out.println("Illegal char: '" + yytext() + "' found.");
             return "";
           }
