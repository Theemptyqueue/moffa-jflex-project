package comboscanner;

/**
 *
 * @author John David Moffa Original code provided by Dr. Steinmetz
 */
public class SimpleScannerMain {

    /**
     * @param argv the command line arguments
     */
    public static void main(String argv[]) {
        
     if (argv.length == 0) {
      System.out.println("Usage : java Scannerv1 [ --encoding <name> ] <inputfile(s)>");
    }
    else {
      int firstFilePos = 0;
      String encodingName = "UTF-8";
      if (argv[0].equals("--encoding")) {
        firstFilePos = 2;
        encodingName = argv[1];
        try {
          java.nio.charset.Charset.forName(encodingName); // Side-effect: is encodingName valid? 
        } catch (Exception e) {
          System.out.println("Invalid encoding '" + encodingName + "'");
          return;
        }
      }
      
      for (int i = firstFilePos; i < argv.length; i++) {
        DependantScannerv1 scanner = null;
        try {
          java.io.FileInputStream stream = new java.io.FileInputStream(argv[i]);
          java.io.Reader reader = new java.io.InputStreamReader(stream, encodingName);
          scanner = new DependantScannerv1(reader);
          SimpleScanner s1 = new SimpleScanner("This is a dummy string");
          Token t;
          while ( !scanner.zzAtEOF ) {
              String inlex = scanner.nextToken();
              //check to see if the input string in null
              if( inlex != null ){
                  //check to see if the input string has a lenght of zero
                  if( inlex.length() != 0){
                      // run if the string is not null and the string has a length greater than zero.
                       t = s1.assignToken(inlex);
                       System.out.println("Token is " + t.type + ":" + t.lexeme);
                  }
              }
          }
        }
        catch (java.io.FileNotFoundException e) {
          System.out.println("File not found : \""+argv[i]+"\"");
        }
        catch (java.io.IOException e) {
          System.out.println("IO error scanning file \""+argv[i]+"\"");
          System.out.println(e);
        }
        catch (Exception e) {
          System.out.println("Unexpected exception:");
          e.printStackTrace();
        }
      }
    }
       
    
    }

}
