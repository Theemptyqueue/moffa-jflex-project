/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package comboscanner;

/**
 *
 * @author John David Moffa Original code provided by Dr. Steinmetz
 */
public enum TokenType {
    NUMBER, ID, PLUS, MINUS, SEMI, ASSIGN, WHILE, PROGRAM, LEFTPAREN, RIGHTPAREN,
    LEFTSQUARE, RIGHTSQUARE, LEFTCURLY, RIGHTCURLY, AND, NOT, OR, CHEESEGRATER, 
    LESSER, MULTIPLY, INTDIVIDE,REALDIVIDE, ASSIGNMENT, LESSER_EQUAL,
    CHEESE_GRATER_EQUAL,LESSEREQUALCOMPLETE,
    CHEESEGRATEREQUALCOMPLETE, ASSIGNCOMPLETE, NOT_EQUAL_TO, CHEESE_GRATER_THEN, 
    LESS_THAN, COLON, CHEESE_GRATER_THEN_EQUAL, LESS_THEN_EQUAL,INTEGER,
    REAL, ARRARY, OF, FUNCTION, PROCEDURE, BEGIN, END, IF, THEN, ELSE,
    DO, VAR, MOD, DIV, DIVIDES,
}
